## Rodando com docker
`docker-compose up`

## Rodando testes usando Docker 
`docker-compose exec app-blog ./vendor/bin/phpunit --testdox`


## Rodando migration e seed

`docker-compose exec app-blog php artisan migrate:fresh --seed`


