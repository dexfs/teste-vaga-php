<?php

use Faker\Generator as Faker;
use Netzee\Domains\Categories\Category;

$factory->define(
    Category::class,
    function (Faker $faker) {
        return [
            'title'       => $faker->title,
            'slug'        => $faker->slug,
            'status'      => $faker->randomElement(['active', 'inactive']),
            'description' => $faker->text(200),
        ];
    }
);

$factory->state(Category::class, 'active', [
    'status' => 'active'
]);

$factory->state(Category::class, 'inactive', [
    'status' => 'inactive'
]);