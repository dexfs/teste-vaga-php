<?php

use Faker\Generator as Faker;
use Netzee\Domains\Categories\Category;
use Netzee\Domains\Posts\Post;

$factory->define(
    Post::class, function (Faker $faker) {
    return [
        'title'       => $faker->text(20),
        'slug'        => $faker->slug,
        'status'      => $faker->randomElement(['active', 'inactive']),
        'description' => $faker->paragraph,
        'cover'       => $faker->imageUrl(),
    ];
});


$factory->afterCreating(Post::class, function ($post){
    $category = factory(Category::class)->create();
    $post->categories()->sync($category);
});

