<?php


namespace NetzeeTests\Unit\Support\Utils;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Netzee\Support\Utils\ResizePostCover;
use NetzeeTests\TestCase;

class ResizePostCoverTest extends TestCase
{
    /**
     * @
     */
    public function testShouldResizeAndSaveImage()
    {
        $this->markTestSkipped('Necessário ajustar pra não salvar ou deletar os arquivos');
        Storage::fake('public');
        $image = UploadedFile::fake()->image('avatar.jpg', 462, 432);
        $storageFile = Storage::put('covers', $image);
        app(ResizePostCover::class)->__invoke($storageFile);

        Storage::disk('public')->assertExists($storageFile);
    }
}