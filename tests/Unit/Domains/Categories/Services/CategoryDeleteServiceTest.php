<?php

namespace NetzeeTests\Unit\Domains\Categories\Services;

use Netzee\Domains\Categories\Category;
use Netzee\Domains\Categories\Services\CategoryDeleteService;
use NetzeeTests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;


class CategoryDeleteServiceTest extends TestCase
{
    use RefreshDatabase;

    public function testShouldDeleteCategory()
    {
        $categoryFactory = factory(Category::class)->create();
        app(CategoryDeleteService::class)->__invoke($categoryFactory->id);

        $category = app(Category::class)->first();
        $categoryDeleted = app(Category::class)
            ->withTrashed()
            ->where('id', $categoryFactory->id)
            ->first();

        $this->assertNull($category);
        $this->assertNotNull($categoryDeleted->deleted_at);
    }

    public function testShouldNotDeleteCategoryWithInvalidId()
    {
        $countDeleted = app(CategoryDeleteService::class)->__invoke(-1);
        $this->assertEquals(0, $countDeleted);
    }
}