<?php

namespace NetzeeTests\Unit\Domains\Categories\Services;

use Netzee\Domains\Categories\Category;
use Netzee\Domains\Categories\Services\CategoryUpdateService;
use NetzeeTests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryUpdateServiceTest extends TestCase
{
    use RefreshDatabase;

    public function testUpdateCategory()
    {
        factory(Category::class)->create();
        $category = app(Category::class)->first();

        app(CategoryUpdateService::class)->__invoke(
            $category->id,
            [
                'title'       => 'Design 2',
                'description' => 'Design Description',
                'status'      => 'active',
            ]
        );

        $this->assertInstanceOf(Category::class, $category);
        $this->assertDatabaseHas(
            'categories',
            [
                'title'       => 'Design 2',
                'description' => 'Design Description',
                'status'      => 'active',
                'slug'        => 'design-2',
            ]
        );
    }

    public function testCreateAnInvalidCategory()
    {
        factory(Category::class)->create();
        $category = app(Category::class)->first();

        $this->expectException(\PDOException::class);
        app(CategoryUpdateService::class)->__invoke(
            $category->id,
            [
                'title'       => null,
                'description' => 'Design Description',
            ]
        );
    }
}