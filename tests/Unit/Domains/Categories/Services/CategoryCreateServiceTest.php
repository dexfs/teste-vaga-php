<?php

namespace NetzeeTests\Unit\Domains\Categories\Services;

use Netzee\Domains\Categories\Category;
use Netzee\Domains\Categories\Services\CategoryCreateService;
use NetzeeTests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryCreateServiceTest extends TestCase
{
    use RefreshDatabase;

    public function testCreateAValidCategory()
    {
        $category = app(CategoryCreateService::class)->__invoke(
            [
                'title'       => 'Design',
                'description' => 'Design Description',
                'status'      => 'active',
            ]
        );

        $this->assertInstanceOf(Category::class, $category);
        $this->assertDatabaseHas(
            'categories',
            [
                'title'       => 'Design',
                'description' => 'Design Description',
                'status'      => 'active',
                'slug'        => 'design',
            ]
        );
    }

    public function testCreateAnInvalidCategory()
    {
        $this->expectException(\PDOException::class);
        app(CategoryCreateService::class)->__invoke(
            [
                'description' => 'Design Description',
                'status'      => 'active',
            ]
        );

    }
}