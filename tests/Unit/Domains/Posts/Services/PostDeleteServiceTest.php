<?php

namespace NetzeeTests\Unit\Domains\Posts\Services;

use Netzee\Domains\Categories\Category;
use NetzeeTests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Netzee\Domains\Posts\Post;
use Netzee\Domains\Posts\Services\PostDeleteService;

/**
 * Class PostDeleteServiceTest
 * @package NetzeeTests\Unit\Domains\Posts\Services
 * @group Post
 */
class PostDeleteServiceTest extends TestCase
{
    use RefreshDatabase;

    public function testShouldDeletePostCorrectly()
    {
        factory(Category::class)->create();
        $post = factory(Post::class)->create();
        $deleted = app(PostDeleteService::class)->__invoke($post->id);
        $this->assertEquals(1, $deleted);
    }
}