<?php

namespace NetzeeTests\Unit\Domains\Posts\Services;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Netzee\Domains\Categories\Category;
use Netzee\Domains\Posts\Post;
use Netzee\Domains\Posts\Services\PostUpdateService;
use NetzeeTests\TestCase;

class PostUpdateServiceTest extends TestCase
{
    use RefreshDatabase;

    public function testShouldUpdateAPost()
    {
        factory(Category::class)->create();
        $post        = factory(Post::class)->create();

        $updatedPost = [
            'title'      => 'Updated Post',
            'status'     => 'inactive',
            'categories' => $post->categories()->pluck('id'),
        ];

        app(PostUpdateService::class)->__invoke(
            $post->id,
            $updatedPost
        );

        $this->assertDatabaseHas('posts', collect($updatedPost)->except('categories')->toArray());
    }
}