<?php


namespace NetzeeTests\Unit\Domains\Posts\Services;

use Netzee\Domains\Categories\Category;
use NetzeeTests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Netzee\Domains\Posts\Post;
use Netzee\Domains\Posts\Services\PostCreateService;

class PostCreateServiceTest extends TestCase
{
    use RefreshDatabase;

    public function testShouldCreateAValidPost()
    {
        $category = factory(Category::class)->create();
        $newPost = [
            'title' => 'New Post',
            'status' => 'active',
            'description' => 'Description Post',
            'categories' => [ 0 => $category->id ],
        ];
        $post = app(PostCreateService::class)->__invoke($newPost);

        $this->assertInstanceOf(Post::class, $post);
        $this->assertDatabaseHas('posts', collect($newPost)->except('categories')->toArray());
    }
}