<?php

namespace Netzee\Units\Admin\Auth\Http\Routes;

use Netzee\Support\Http\Routing\RouteFile;

class Web extends RouteFile
{
    protected function routes()
    {
        $this->registerDefaultRoutes();
    }

    protected function registerDefaultRoutes()
    {
        $this->router->group(['prefix' => 'admin'], function (){
            $this->auth();
        });
    }

    protected function auth()
    {
        $this->router->group(['prefix' => 'auth'], function () {
            $this->router->get('login', 'LoginController@showLoginForm')->name('show.login');
            $this->router->post('login', 'LoginController@login')->name('login');
            $this->router->post('logout', 'LoginController@logout')->name('logout');
            $this->router->get('register', 'RegisterController@showRegistrationForm')->name('show.register');
            $this->router->post('register', 'RegisterController@register')->name('register');
        });
    }
}