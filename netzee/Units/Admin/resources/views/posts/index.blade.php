@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" style="display: flex; flex-direction: row; justify-content: space-between ">
                        <div>Posts</div>
                        <div>
                            <a href="{{ route('admin.posts.create.index') }}"> Criar </a>
                        </div>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                            <table class="table">
                                <thead class="thead-dark">

                                <tr>
                                    <th>Titulo</th>
                                    <th colspan="2">Descrição</th>
                                </tr>
                                </thead>
                                @forelse($posts as $post)
                                    <tr>
                                        <td>
                                            {{ $post->title }} <br/>
                                        </td>
                                        <td>
                                            {{ $post->description }}
                                            <br />
                                            <div class="badge">{{ $post->created_at->diffForHumans() }}</div>
                                        </td>
                                        <td style="display: flex; flex-direction: row; justify-content: space-between">
                                            <a href="{{ route('admin.posts.edit.index',['id' => $post->id]) }}" style="margin-right: 5px" class="badge badge-info">
                                                <span class="oi oi-text"></span>
                                            </a>
                                            <!-- Button trigger modal -->
                                            <a href="#" data-toggle="modal" data-target="#delete-post-{{ $post->id }}" class="badge badge-danger">
                                                <span class="oi oi-trash"></span>
                                            </a>

                                            @includeIf('admin::posts/forms.modal', [
                                                'target' => "delete-post-$post->id",
                                                'post' => $post,
                                                'modalTitle' => 'Deletar Post',
                                                'endpoint' => 'admin.posts.destroy'
                                            ])
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="2"> Não existem postagens </td>
                                    </tr>
                                @endforelse
                            </table>
                            {{ $posts->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
