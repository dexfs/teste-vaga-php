{!! Form::model($post, ['url' => route($formUrl), 'files' => true]) !!}
@php
    $classErrorTitle = $errors->has('title') ? ' is-invalid' : '';
    $classErrorDescription = $errors->has('description') ? ' is-invalid' : '';
    $classErrorCategories = $errors->has('categories') ? ' is-invalid' : '';
@endphp
<div class="form-group">
    {!! Form::label('title', 'Título'); !!}
    {{ method_field($formMethod) }}
    @if($formMethod === 'PUT')
        {!! Form::hidden('id', $post->id) !!}
    @endif
    {!! Form::text('title', null, ['class' => "form-control $classErrorTitle", 'placeholder' => 'Título']) !!}
    <small id="emailHelp" class="form-text text-muted">Defina o título do seu post  de forma que ele seja achado facilmente</small>
    @if ($errors->has('title'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('title') }}</strong>
        </span>
    @endif
</div>

<div class="form-group">
    {!! Form::label('description', 'Descrição'); !!}
    {!! Form::textArea('description', null, ['class' => "form-control $classErrorDescription", 'placeholder' => 'Descrição']) !!}
    @if ($errors->has('description'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('description') }}</strong>
        </span>
    @endif
</div>

<div class="form-group">
    {!! Form::label('categories', 'Categoria'); !!}
    {!! Form::select('categories[]', $categories, null, ['class' => "form-control $classErrorCategories", 'multiple']) !!}
    @if ($errors->has('categories'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('categories') }}</strong>
        </span>
    @endif
</div>

<div class="form-group">
    {!! Form::select('status', ['active' => 'Ativo', 'inactive' => 'Inativo'], null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cover', 'Capa'); !!}
    <div>
        {!! Form::file('cover', null, ['class' => "form-control $classErrorDescription"]) !!}
        @if($formMethod === 'PUT')
            <small id="emailHelp" class="form-text text-muted">Ao escolher uma nova capa, ela substituirá a atual</small>
        @endif
        @if ($errors->has('cover'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('cover') }}</strong>
            </span>
        @endif
        @if($post !== null && $post->cover !== null)
            <div style="display: flex; flex-direction: column;margin-top: 10px">
            <img src="{{ Storage::url($post->cover) }}" alt="{{ $post->title }}" with="{{ Storage::size($post->cover) }}"/>
            <span>
                <a href="{{ route('admin.posts.image.destroy', ['id' => $post->id ]) }}" onclick="return confirm('Deseja realmente remover a capa do post')">
                    remover
                </a>
            </span>
            </div>
        @endif    
    </div>

</div>

{!! Form::submit('Salvar', ['class'=> 'btn btn-primary']) !!}
{!! Form::close()  !!}


