<!-- Modal -->
<div class="modal fade" id="{{ $target }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ $modalTitle ?? 'Modal' }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Deseja realmente remover esse post?
                <h3 class="display-4">{{$post->title}}</h3>
                <span>Criação: {{ $post->created_at->diffForHumans() }}</span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-post-{{ $post->id }}" data-dismiss="modal">Voltar</button>
                <button type="button" class="btn btn-primary btn-post-{{ $post->id }}" data-url="{{ route($endpoint) }}" data-id="{{ $post->id }}">Confirmar</button>
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script>
    $('.btn-post-{{ $post->id }}').on('click', function(e){
        const button  = $(this);
        const buttonCancel = $('.btn-post-{{ $post->id }}');
        const id = button.data('id');
        const url = button.data('url');
        button.attr('disabled', true);
        buttonCancel.attr('disabled', true);
        button.text('Aguarde...');
        axios.delete(url, { data: {id} })
            .then( data =>  window.location = data.data.endpointReturn)
            .catch( error => {
                console.log(error)
                button.attr('disabled', false);
                button.text('Confirmar');
                buttonCancel.attr('disabled', false);
            });
        event.preventDefault();
    })
</script>