@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Editar post</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @include('admin::posts/forms.form', [
                            'errors' => $errors,
                            'formUrl' => 'admin.posts.edit.update',
                            'post' => $post,
                            'categories' => $categories,
                            'formMethod' => 'PUT'
                        ])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection