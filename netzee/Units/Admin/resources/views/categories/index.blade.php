@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" style="display: flex; flex-direction: row; justify-content: space-between ">
                        <div>Categorias</div>
                        <div>
                            <a href="{{ route('admin.categories.create.index') }}"> Criar </a>
                        </div>

                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                     <table class="table">
                         <thead class="thead-dark">

                         <tr>
                             <th>Titulo</th>
                             <th colspan="2">Descrição</th>
                         </tr>
                         </thead>
                         @forelse($categories as $category)
                             <tr>
                                <td>
                                    {{ $category->title }} <br/>
                                </td>
                                <td>
                                    {{ $category->description }}
                                    <br />
                                    <div class="badge">{{ $category->created_at->diffForHumans() }}</div>
                                </td>
                                <td style="display: flex; flex-direction: row; justify-content: space-between">
                                    <a href="{{ route('admin.categories.edit.index',['id' => $category->id]) }}" style="margin-right: 5px" class="badge badge-info">
                                        <span class="oi oi-text"></span>
                                    </a>

                                    <!-- Button trigger modal -->
                                    <a href="#" data-toggle="modal" data-target="#delete-category-{{ $category->id }}" class="badge badge-danger">
                                        <span class="oi oi-trash"></span>
                                    </a>

                                    @includeIf('admin::categories/forms.modal', [
                                        'target' => "delete-category-$category->id",
                                        'category' => $category,
                                        'modalTitle' => 'Deletar categoria',
                                        'endpoint' => 'admin.categories.destroy'
                                    ])
                                </td>
                             </tr>
                         @empty
                             <tr>
                                <td colspan="2"> Não existem categorias </td>
                             </tr>
                         @endforelse
                     </table>
                    {{ $categories->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection