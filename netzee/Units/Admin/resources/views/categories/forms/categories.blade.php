{!! Form::model($category, ['url' => route($formUrl)]) !!}
@php
    $classErrorTitle = $errors->has('title') ? ' is-invalid' : '';
    $classErrorDescription = $errors->has('description') ? ' is-invalid' : '';
@endphp
<div class="form-group">
    {!! Form::label('title', 'Título'); !!}
    {{ method_field($formMethod) }}
    @if($formMethod === 'PUT')
        {!! Form::hidden('id', $category->id) !!}
    @endif
    {!! Form::text('title', null, ['class' => "form-control $classErrorTitle", 'placeholder' => 'Título']) !!}
    <small id="emailHelp" class="form-text text-muted">Defina o título do seu post  de forma que ele seja achado facilmente</small>
    @if ($errors->has('title'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('title') }}</strong>
        </span>
    @endif
</div>

<div class="form-group">
    {!! Form::label('description', 'Descrição'); !!}
    {!! Form::textArea('description', null, ['class' => "form-control $classErrorDescription", 'placeholder' => 'Descrição']) !!}
    @if ($errors->has('description'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('description') }}</strong>
        </span>
    @endif

</div>
<div class="form-group">
    {!! Form::select('status', ['active' => 'Ativo', 'inactive' => 'Inativo'], null, ['class' => 'form-control']) !!}
</div>
{!! Form::submit('Salvar', ['class'=> 'btn btn-primary']) !!}
{!! Form::close()  !!}


