<?php


namespace Netzee\Units\Admin\Home\Http\Routes;

use Netzee\Support\Http\Routing\RouteFile;

class Web extends RouteFile
{
    protected function routes()
    {
        $this->registerDefaultRoutes();
    }

    protected function registerDefaultRoutes()
    {
        $this->router->group(['prefix' => 'admin'], function (){
            $this->home();
        });
    }

    protected function home()
    {
        $this->router->get('/', 'IndexController@index')
            ->middleware('auth');
    }
}