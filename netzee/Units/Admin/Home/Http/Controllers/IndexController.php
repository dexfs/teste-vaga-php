<?php

namespace Netzee\Units\Admin\Home\Http\Controllers;

use Netzee\Support\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        return view('admin::home');
    }
}