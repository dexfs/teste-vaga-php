<?php

namespace Netzee\Units\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Netzee\Units\Admin\Auth\Providers\AuthRouterServiceProvider;
use Netzee\Units\Admin\Categories\Providers\CategoriesRouterServiceProvider;
use Netzee\Units\Admin\Home\Providers\HomeRouterServiceProvider;
use Netzee\Units\Admin\Posts\Providers\PostsRouterServiceProvider;

class UnitServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->register(CategoriesRouterServiceProvider::class);
        $this->app->register(PostsRouterServiceProvider::class);
        $this->app->register(HomeRouterServiceProvider::class);
        $this->app->register(AuthRouterServiceProvider::class);
        $this->app->register(ViewServiceProvider::class);
    }

}