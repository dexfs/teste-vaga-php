<?php

namespace Netzee\Units\Admin\Posts\Http\Controllers;

use Netzee\Domains\Posts\Services\PostUpdateService;
use Netzee\Support\Http\Controllers\Controller;

class PostsCoverDeleteController extends Controller
{
    public function destroy()
    {
        try{
            $postId = request()->input('id');
            app(PostUpdateService::class)->__invoke($postId, [
                'cover' => null
            ]);

            return redirect()->back()->withInput();
        }catch (\Exception $e){
            return redirect()->back()
                ->withInput(request()->all())
                ->withErrors('Remove Image', $e->getMessage());
        }
    }
}