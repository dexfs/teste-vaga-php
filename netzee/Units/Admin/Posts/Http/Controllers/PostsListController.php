<?php


namespace Netzee\Units\Admin\Posts\Http\Controllers;

use Netzee\Domains\Posts\Post;
use Netzee\Support\Http\Controllers\Controller;

class PostsListController extends Controller
{
    public function index()
    {
        $posts = Post::withTrashed()->paginate(10);
        return view('admin::posts.index', ['posts' => $posts]);
    }
}