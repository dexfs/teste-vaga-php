<?php


namespace Netzee\Units\Admin\Posts\Http\Controllers;

use Netzee\Domains\Categories\Category;
use Netzee\Domains\Posts\Post;
use Netzee\Domains\Posts\Services\PostUpdateService;
use Netzee\Support\Http\Controllers\Controller;
use Netzee\Support\Utils\ResizePostCover;

class PostsUpdateController extends Controller
{
    public function index()
    {
        $categories = Category::where('status', 'active')->pluck('title', 'id');
        $post = app(Post::class)->with('categories')->find(request()->input('id'));
        return view('admin::posts.edit', [ 'post' => $post, 'categories' => $categories ]);
    }

    public function update()
    {
        $id = request()->input('id');
        $data = request()->except('id');
        if($cover = $this->getImagePath()){
            $data['cover'] = $cover;
            app(ResizePostCover::class)->__invoke($cover);
        }

        app(PostUpdateService::class)->__invoke($id, $data);
        return redirect()->route('admin.posts.index');
    }

    private function getImagePath()
    {
        return request()->hasFile('cover') ?
            request()->file('cover')->store('covers') : null;
    }

}