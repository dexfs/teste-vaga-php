<?php

namespace Netzee\Units\Admin\Posts\Http\Controllers;

use Netzee\Domains\Posts\Services\PostDeleteService;
use Netzee\Support\Http\Controllers\Controller;

class PostsDestroyController extends Controller
{
    public function destroy()
    {
        app(PostDeleteService::class)->__invoke(request()->input('id'));
        return response()->json([
            'endpointReturn' => route('admin.posts.index')
        ]);
    }

}