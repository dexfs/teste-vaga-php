<?php

namespace Netzee\Units\Admin\Posts\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Netzee\Domains\Categories\Category;
use Netzee\Domains\Posts\Services\PostCreateService;
use Netzee\Support\Http\Controllers\Controller;
use Netzee\Support\Utils\ResizePostCover;

class PostsCreateController extends Controller
{
    public function index()
    {
        $categories = Category::where('status', 'active')->pluck('title', 'id');

        return view(
            'admin::posts.create',
            [
                'categories' => $categories,
            ]
        );
    }

    public function store()
    {
        $this->validator(request()->all())->validate();
        $params          = request()->all();
        $params['cover'] = $this->getImagePath();
        if ($params['cover']) {
            app(ResizePostCover::class)->__invoke($params['cover']);
        }
        app(PostCreateService::class)->__invoke($params);

        return redirect()->route('admin.posts.index');
    }

    protected function validator(array $data)
    {
        $messages = [
            'required' => 'O campo :attribute é obrigatório',
        ];

        return Validator::make(
            $data,
            [
                'title'       => ['required', 'string', 'max:255', 'unique:posts'],
                'categories'  => ['required', 'array'],
                'description' => ['required', 'string'],
            ],
            $messages
        );
    }

    private function getImagePath()
    {
        return request()->hasFile('cover') ?
            request()->file('cover')->store('covers') : null;
    }

}
