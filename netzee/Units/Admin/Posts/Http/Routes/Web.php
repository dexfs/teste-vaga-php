<?php

namespace Netzee\Units\Admin\Posts\Http\Routes;

use Netzee\Support\Http\Routing\RouteFile;

class Web extends RouteFile
{
    protected function routes()
    {
        $this->registerDefaultRoutes();
    }

    protected function registerDefaultRoutes()
    {
        $this->router->name('admin.posts.')->group(function () {
            $this->router->group(['prefix' => 'admin'], function (){
                $this->posts();
            });
        });
    }

    private function posts()
    {
        $this->router->group(
            ['prefix' => 'posts'],
            function () {
                $this->router->resource('/', 'PostsListController')->only(
                    [
                        'index',
                    ]
                );

                $this->router->resource('create', 'PostsCreateController')->only(
                    [
                        'index',
                        'store'
                    ]
                );

                $this->router->get('edit', 'PostsUpdateController@index')->name('edit.index');
                $this->router->put('edit', 'PostsUpdateController@update')->name('edit.update');

                $this->router->delete('destroy', 'PostsDestroyController@destroy')->name('destroy');
                $this->router->get('image/delete', 'PostsCoverDeleteController@destroy')->name('image.destroy');
            }
        );
    }
}