<?php

namespace Netzee\Units\Admin\Categories\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Netzee\Domains\Categories\Services\CategoryCreateService;
use Netzee\Support\Http\Controllers\Controller;

class CategoriesCreateController extends Controller
{
    public function index()
    {
        return view('admin::categories.create');
    }

    public function store(Request $request)
    {
        $this->validator($request->all())->validate();
        app(CategoryCreateService::class)->__invoke($request->all());
        return redirect()->route('admin.categories.index');
    }


    protected function validator(array $data)
    {
        $messages = [
            'required' => 'O campo :attribute é obrigatório'
        ];

        return Validator::make($data, [
            'title' => ['required', 'string', 'max:255'],
        ], $messages);
    }
}