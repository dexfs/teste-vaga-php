<?php

namespace Netzee\Units\Admin\Categories\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Netzee\Domains\Categories\Category;
use Netzee\Domains\Categories\Services\CategoryUpdateService;
use Netzee\Support\Http\Controllers\Controller;

class CategoriesUpdateController extends Controller
{

    public function index()
    {
        $category = app(Category::class)->find(request()->input('id'));
        return view('admin::categories.edit', [
            'category' => $category
        ]);

    }
    public function update()
    {
        $this->validator(request()->all());
        app(CategoryUpdateService::class)->__invoke(request()->get('id'), request()->except('id'));
        return redirect()->route('admin.categories.index');
    }

    protected function validator(array $data)
    {
        $messages = [
            'required' => 'O campo :attribute é obrigatório'
        ];

        return Validator::make($data, [
            'title' => ['sometimes', 'required', 'string', 'max:255', 'unique:categories'],
            'description' => ['sometimes', 'required', 'string'],
        ], $messages);
    }
}