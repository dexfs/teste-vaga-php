<?php


namespace Netzee\Units\Admin\Categories\Http\Controllers;

use Netzee\Domains\Categories\Category;
use Netzee\Support\Http\Controllers\Controller;

class CategoriesListController extends Controller
{
    public function index()
    {
        $categories = Category::paginate(10);
        return view('admin::categories.index', ['categories' => $categories]);
    }
}