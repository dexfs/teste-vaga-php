<?php

namespace Netzee\Units\Admin\Categories\Http\Controllers;

use Netzee\Domains\Categories\Services\CategoryDeleteService;
use Netzee\Support\Http\Controllers\Controller;

class CategoriesDestroyController extends Controller
{
    public function destroy()
    {
        app(CategoryDeleteService::class)->__invoke(request()->input('id'));
        return response()->json([
            'endpointReturn' => route('admin.categories.index')
        ]);
    }
}