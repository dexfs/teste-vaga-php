<?php

namespace Netzee\Units\Admin\Categories\Http\Routes;

use Netzee\Support\Http\Routing\RouteFile;

class Web extends RouteFile
{
    protected function routes()
    {
        $this->registerDefaultRoutes();
    }

    protected function registerDefaultRoutes()
    {
        $this->router->name('admin.categories.')->group(function () {
            $this->router->group(['prefix' => 'admin'], function (){
                $this->categories();
            });
        });
    }

    protected function categories()
    {
        $this->router->group(['prefix' => 'categories'], function () {
                $this->router->resource('/', 'CategoriesListController')
                    ->only([
                        'index',
                    ]);

                $this->router->resource('create', 'CategoriesCreateController')
                    ->only([
                        'index',
                        'store'
                    ]);

                $this->router->get('edit', 'CategoriesUpdateController@index')->name('edit.index');
                $this->router->put('edit', 'CategoriesUpdateController@update')->name('edit.update');

                $this->router->delete('destroy', 'CategoriesDestroyController@destroy')->name('destroy');
            }
        );
    }
}