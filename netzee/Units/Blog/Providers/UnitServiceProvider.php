<?php

namespace Netzee\Units\Blog\Providers;

use Illuminate\Support\ServiceProvider;
use Netzee\Units\Blog\Index\Providers\BlogRouterServiceProvider;

class UnitServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->register(BlogRouterServiceProvider::class);
        $this->app->register(ViewServiceProvider::class);
    }

}