@extends('layouts.blog')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @foreach($posts as $post)
            <div class="col-md-8">
                <div class="card" style="margin-bottom: 5px !important;margin-top: 5px !important;">
                    <div class="card-header">
                        <a href="{{ route('blog.detail', $post->slug) }}">{{ $post->title }}</a>
                    </div>

                    <div class="card-body">
                        {{ $post->description }}
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="row justify-content-center">
            {{ $posts->links() }}
        </div>
    </div>
@endsection