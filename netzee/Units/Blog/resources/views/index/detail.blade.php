@extends('layouts.blog')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card" style="margin-bottom: 5px !important;margin-top: 5px !important;">
                    <div class="card-header">{{ $post->title }}</div>

                    <div class="card-body">
                        @if($post->cover !== null && Storage::exists($post->cover))
                            <div style="display: flex; flex-direction: column; margin-bottom: 10px">
                                <img src="{{ Storage::url($post->cover) }}" alt="{{ $post->title }}" with="{{ Storage::size($post->cover) }}"/>
                            </div>
                        @endif
                        {{ $post->description }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection