<?php


namespace Netzee\Units\Blog\Index\Http\Routes;

use Netzee\Support\Http\Routing\RouteFile;

class Web extends RouteFile
{
    protected function routes()
    {
        $this->registerDefaultRoutes();
    }

    protected function registerDefaultRoutes()
    {
        $this->blog();
    }

    protected function blog()
    {
        $this->router->get('/', 'IndexController@index')->name('blog.index');
        $this->router->get('/{slug}', 'IndexController@detail')->name('blog.detail');
    }
}