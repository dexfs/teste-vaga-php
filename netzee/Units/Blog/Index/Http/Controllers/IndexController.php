<?php

namespace Netzee\Units\Blog\Index\Http\Controllers;

use Netzee\Domains\Posts\Post;
use Netzee\Support\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $posts = app(Post::class)->where('status', 'active')->paginate();
        return view('blog::index.posts', ['posts' => $posts]);
    }

    public function detail($slug)
    {
        $post = app(Post::class)
            ->where('status', 'active')
            ->where('slug', $slug)
            ->first();

        return view('blog::index.detail', ['post' => $post]);

    }
}