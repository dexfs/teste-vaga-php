<?php

namespace Netzee\Support\Utils;

use Illuminate\Support\Facades\Storage;
use Spatie\Image\Image;
use Spatie\Image\Manipulations;

class ResizePostCover
{
    public function __invoke($image)
    {
        try{
            $imageInStorage = storage_path("app/public/$image");
            Image::load($imageInStorage)
                ->fit(Manipulations::FIT_STRETCH, 550, 250)
                ->save($imageInStorage);
        }catch (\Exception $e){
            logger()->error("{$e->getMessage()}");
            throw $e;
        }
    }
}