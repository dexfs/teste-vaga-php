<?php


namespace Netzee\Support\Utils;


use Illuminate\Support\Facades\Storage;

class DeleteOldFileService
{
    public function __invoke($image)
    {
        return Storage::delete($image);
    }
}