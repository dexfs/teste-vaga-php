<?php


namespace Netzee\Domains\Categories\Services;


use Netzee\Domains\Categories\Category;

class CategoryCreateService
{
    /**
     * @var \Netzee\Domains\Categories\Category
     */
    private $category;

    /**
     * CategoryCreateService constructor.
     *
     * @param \Netzee\Domains\Categories\Category $category
     */
    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function __invoke($data)
    {
        return $this->category->create($data);
    }
}