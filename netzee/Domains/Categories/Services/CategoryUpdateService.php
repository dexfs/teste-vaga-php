<?php


namespace Netzee\Domains\Categories\Services;

use Netzee\Domains\Categories\Category;

class CategoryUpdateService
{
    /**
     * @var \Netzee\Domains\Categories\Category
     */
    private $category;

    /**
     * CategoryCreateService constructor.
     *
     * @param \Netzee\Domains\Categories\Category $category
     */
    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function __invoke($id, $data)
    {
        $model = $this->category->find($id);
        $model->fill($data);
        $model->save();
        return $model->fresh();
    }

}