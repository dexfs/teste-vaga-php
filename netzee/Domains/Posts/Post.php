<?php


namespace Netzee\Domains\Posts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Netzee\Domains\Categories\Category;
use Emadadly\LaravelUuid\Uuids;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;


class Post extends Model
{
    use Uuids, HasSlug, SoftDeletes;

    protected $fillable = [
        'id',
        'title',
        'status',
        'description',
        'cover',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $incrementing = false;

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    public function categories()
    {
        return $this->belongsToMany(
            Category::class,
            'post_categories',
            'post_id',
            'category_id');
    }
}