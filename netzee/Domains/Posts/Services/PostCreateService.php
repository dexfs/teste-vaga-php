<?php

namespace Netzee\Domains\Posts\Services;

use Netzee\Domains\Posts\Post;

class PostCreateService
{
    /**
     * @var \Netzee\Domains\Posts\Post
     */
    private $post;

    /**
     * PostCreateService constructor.
     *
     * @param \Netzee\Domains\Posts\Post $post
     */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function __invoke($data)
    {
        try {
            $post = $this->post->create(collect($data)->except('categories')->toArray());
            $post->categories()->attach(collect($data['categories'])->values());
            return $post;
        } catch (\Exception $e) {
            dd($e);
            return (string) $e;
        }
    }
}