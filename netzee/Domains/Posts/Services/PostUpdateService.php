<?php

namespace Netzee\Domains\Posts\Services;

use Netzee\Domains\Posts\Post;
use Netzee\Support\Utils\DeleteOldFileService;

class PostUpdateService
{
    /**
     * @var \Netzee\Domains\Posts\Post
     */
    private $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function __invoke($id, $data)
    {
        $model = $this->post->find($id);
        $cover = $model->cover;
        $model->fill($data);
        $model->save();

        $categoriesFromForm = collect($data['categories']);
        $model->categories()->sync($categoriesFromForm->values());
        if (isset($data['cover'])) {
            app(DeleteOldFileService::class)->__invoke($cover);
        }

        return $model;
    }

}