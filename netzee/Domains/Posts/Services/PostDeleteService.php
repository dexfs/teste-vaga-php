<?php

namespace Netzee\Domains\Posts\Services;

use Netzee\Domains\Posts\Post;

class PostDeleteService
{
    /**
     * @var \Netzee\Domains\Posts\Post
     */
    private $post;

    /**
     * PostDeleteService constructor.
     *
     * @param \Netzee\Domains\Posts\Post $post
     */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function __invoke($id)
    {
        return $this->post->destroy($id);
    }
}